from AccelBrainBeat.brainbeat.monaural_beat import MonauralBeat

brain_beat = MonauralBeat() # for monaural beats

# general anxiety aid monaural soft beat for rpi
brain_beat.save_beat(
    output_file_name="prolong_anxiety_aid_rpi_soft_monaural_beat.wav",
    # soft_max == 432; soft_min == 212; hard_min == 20;
    frequencys=(423.6, 432), # (left, right); diff => beat freq;
    play_time=9, # in seconds, keep it low since system does not have much memory
    volume=0.09 # keep it 0.03 , 0.06 or 0.09
)
