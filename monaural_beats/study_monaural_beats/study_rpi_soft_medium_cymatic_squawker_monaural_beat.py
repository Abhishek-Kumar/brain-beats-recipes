from AccelBrainBeat.brainbeat.monaural_beat import MonauralBeat

brain_beat = MonauralBeat() # for monaural beats

#study monaural soft medium cymatic squawker beat for rpi
brain_beat.save_beat(
    output_file_name="study_rpi_soft_medium_cymatic_squawker_monaural_beat.wav",
    # soft_max == 432; soft_min == 253 ; hard_min == 250;
    frequencys=(253, 259), # (left, right); diff => beat freq;
    play_time=9, # in seconds, keep it low since system does not have much memory
    volume=0.39 # keep it 0.3 , 0.6 or 0.9
)
