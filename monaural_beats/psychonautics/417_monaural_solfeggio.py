from AccelBrainBeat.brainbeat.monaural_beat import MonauralBeat

brain_beat = MonauralBeat() # for monaural beats.
brain_beat.save_beat(
    output_file_name="417_monaural_solfeggio.wav",
    frequencys=(417, 417), # (left, right); diff => beat freq;
    play_time=10, # in seconds
    volume=0.09 # depends on system environment
)
