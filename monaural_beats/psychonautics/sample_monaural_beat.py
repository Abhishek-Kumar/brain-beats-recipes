from AccelBrainBeat.brainbeat.monaural_beat import MonauralBeat

brain_beat = MonauralBeat() # for monaural beats.
brain_beat.save_beat(
    output_file_name="sample_monaural_beat.wav",
    frequencys=(400, 430), # (left, right); diff => beat freq;
    play_time=10, # in seconds
    volume=0.01 # depends on system environment
)
