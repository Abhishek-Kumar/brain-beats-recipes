from AccelBrainBeat.brainbeat.monaural_beat import MonauralBeat

brain_beat = MonauralBeat() # for monaural beats

#sample monaural beat for rpi
brain_beat.save_beat(
    output_file_name="sample_rpi_monaural_beat.wav",
    frequencys=(400, 430), # (left, right); diff => beat freq;
    play_time=9, # in seconds, keep it low since system does not have much memory
    volume=0.03 # keep it 0.03 , 0.06 or 0.09
)
