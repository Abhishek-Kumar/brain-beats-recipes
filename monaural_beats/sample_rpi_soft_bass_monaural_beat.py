from AccelBrainBeat.brainbeat.monaural_beat import MonauralBeat

brain_beat = MonauralBeat() # for monaural beats

#sample monaural soft bass beat for rpi
brain_beat.save_beat(
    output_file_name="sample_rpi_soft_bass_monaural_beat.wav",
    # soft_max == 432; soft_min == 42; hard_min == 40;
    frequencys=(42, 52), # (left, right); diff => beat freq;
    play_time=9, # in seconds, keep it low since system does not have much memory
    volume=0.03 # keep it 0.03 , 0.06 or 0.09
)
