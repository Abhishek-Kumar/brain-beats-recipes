from AccelBrainBeat.brainbeat.binaural_beat import BinauralBeat

brain_beat = BinauralBeat() # for binaural beats.

# for bass soft top spin binaural beats

brain_beat.save_beat(
    output_file_name="s_528_ubuntu_bass_soft_top_spin_binaural_beat.wav",
    # soft_max == 432; soft_min == 42; hard_min = 40;
    frequencys=(42, 560), # (left, right); diff => beat freq;
    play_time=300, # in seconds, keep it less since it does not have much memory
    volume=0.03 # keep it 0.03, 0.06 or 0.09, or experiment for higher
)
