from AccelBrainBeat.brainbeat.binaural_beat import BinauralBeat

brain_beat = BinauralBeat() # for binaural beats.

# sample rpi binaural beat for rpi environment

brain_beat.save_beat(
    output_file_name="sample_rpi_binaural_beat.wav",
    frequencys=(420, 430), # (left, right); diff => beat freq;
    play_time=9, # in seconds, not to keep large amounts since it has less memory
    volume=0.03 # keep it 0.03, 0.06 or 0.09
)
