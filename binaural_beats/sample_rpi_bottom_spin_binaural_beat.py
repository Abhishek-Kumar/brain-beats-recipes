from AccelBrainBeat.brainbeat.binaural_beat import BinauralBeat

brain_beat = BinauralBeat() # for binaural beats.

# sample rpi bottom spin binaural beat
brain_beat.save_beat(
    output_file_name="sample_rpi_bottom_spin_binaural_beat.wav",
    # soft_max == 432; soft_min 212; hard_min = 20;
    frequencys=(432, 422), # (left, right); diff => beat freq;
    play_time=9, # in seconds, keep it low since it does not have much memory
    volume=0.03 # keep it 0.03, 0.06 or 0.09
)
