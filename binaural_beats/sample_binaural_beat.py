from AccelBrainBeat.brainbeat.binaural_beat import BinauralBeat

brain_beat = BinauralBeat() # for binaural beats.

brain_beat.save_beat(
    output_file_name="sample_binaural_beat.wav",
    frequencys=(400, 430), # (left, right); diff => beat freq;
    play_time=10, # in seconds
    volume=0.01 # depends on system environment
)
